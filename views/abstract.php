<?php


abstract class MyAbstractClass{

    protected $name;
    abstract public function getData();
    abstract public function setdata($value);

    public function displayData(){

        echo "Name: ".$this->name;
    }
}//end of MyAbstractClass

class MyClass extends MyAbstractClass{
    public function getData()
    {
        return $this->name;
    }
    public function setdata($value)
    {
        $this->name = $value;
    }
}//end of MyClass

$o = new MyClass();
$o->setdata("BASIS BITM");

echo $o->displayData();